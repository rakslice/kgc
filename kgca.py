import doctest
import os
import sys
import datetime
import time
import traceback

import krpc
import krpc.types
import math

script_path = os.path.dirname(os.path.abspath(__file__))
log_dir = os.path.join(script_path, "log")


FIRST_MANEUVER_ALTITUDE = 12000
TARGET_ORBIT_ALTITUDE = 165000
SLOW_ALTITUDE_MARGIN = 20000  # slow down the burn when we get this close to target altitude
TARGET_APOAPSIS_LEADING_TIME = 30
TARGET_APOAPSIS_LEADING_TIME_MARGIN = 2
HAVE_GANTRY = True


def die(msg, status_code=1):
    print >> sys.stderr, msg
    sys.exit(status_code)


# #### some handy calculation stuff

def true_anomaly_calc(e, ea):
    """
    From the eccentric anomaly (which ksp uses) calculate the true anomaly (which some eqns we have use instead)
    also using the eccentricity.
    :param e: eccentricity, dimensionless
    :param ea: eccentric anomaly, in radians

    A nice test case from the javascript example at http://www.jgiesen.de/kepler/kepler.html
    >>> '%0.4f' % math.degrees(true_anomaly_calc(e=0.5, ea=math.radians(48.43418)))
    '75.8397'
    """
    y = math.sqrt(e + 1) * math.sin(ea/2.)
    x = math.sqrt(1 - e) * math.cos(ea/2.)
    # return 2 * math.atan2(y, x)
    # x = math.sqrt(1.0 - e*e) * math.sin(ea)
    # y = -math.cos(ea) + e
    return 2 * math.atan2(y, x)


def simple_inclination_change_calc(r, theta, gm):
    """
    :param r: orbit radius from center of body, in m
    :param theta: inclination change angle in radians
    :return: magnitude of the change in velocity, in m/s

    >>> '%0.0f' % simple_inclination_change_calc(6378140. + 600000., math.radians(8), 3.986005e14 )
    '1054'
    """
    vi = math.sqrt(gm / r)
    v = 2. * vi * math.sin(theta/2.)
    return v


# #### classes

class Event(object):
    __slots__ = ["condition_func", "callback_func", "check_interval", "next_check_time", "dispose"]

    def __init__(self, condition_func, callback_func, check_interval, next_check_time, dispose):
        self.condition_func = condition_func
        self.callback_func = callback_func
        self.check_interval = check_interval
        self.next_check_time = next_check_time
        self.dispose = dispose


class Automator(object):
    def __init__(self):
        self.currently_polling_events = False

        self.log_filename = None
        """:type: str"""
        self.log_handle = None
        """:type: file"""
        self.conn = None
        """:type: krpc.Connection"""

        self.registered_events = []
        """
        :type: list of Event"""

        print "opening log"
        self.open_log()
        print "opened %s" % self.log_filename

        self.connect()

    def vessel(self):
        """
        :type: krpc.types.Vessel
        """
        return self.conn.space_center.active_vessel

    def target_vessel(self):
        """
        :type: krpc.types.Vessel
        """
        return self.conn.space_center.target_vessel

    def connect(self):
        self.log("connecting to kerbal space program")
        self.conn = krpc.connect(name='kgca')
        self.log("connected")

    def disconnect(self):
        if self.conn is not None:
            self.log("disconnecting from kerbal space program")
            self.conn.close()
            self.conn = None
        else:
            self.log("already disconnected")

    # noinspection PyMethodMayBeStatic
    def get_time(self):
        return time.time()

    def get_simulation_time(self):
        return self.conn.space_center.ut

    def poll_events(self):
        if self.currently_polling_events:
            return

        self.currently_polling_events = True
        try:

            cur_time = self.get_time()

            events_to_remove = []

            for event_index, event in enumerate(self.registered_events):
                if cur_time >= event.next_check_time:
                    if event.condition_func is None:
                        condition_result = True
                    else:
                        # noinspection PyBroadException
                        try:
                            condition_result = event.condition_func()
                        except Exception:
                            self.log("Exception while polling event condition function %r:\n%s" %
                                     (event.condition_func, traceback.format_exc()))
                            continue
                    if condition_result:
                        if event.dispose:
                            events_to_remove.append((event_index, event))
                        else:
                            event.next_check_time += event.check_interval

                        # noinspection PyBroadException
                        try:
                            event.callback_func()
                        except Exception:
                            self.log("Exception while running event callback function %r:\n%s" %
                                     (event.callback_func, traceback.format_exc()))

            for event_index, event in reversed(events_to_remove):
                actual_event = self.registered_events.pop(event_index)
                assert actual_event is event

        finally:
            self.currently_polling_events = False

    def open_log(self):
        if not os.path.isdir(log_dir):
            os.mkdir(log_dir)

        log_filename = None
        i = 0
        while True:
            log_filename = os.path.join(log_dir, "%d.log" % i)
            if not os.path.exists(log_filename):
                break
            i += 1

        self.log_filename = log_filename
        self.log_handle = open(log_filename, "w")

    def log(self, msg):
        formatted_msg = u"%s: %s" % (datetime.datetime.now().isoformat(), msg)
        encoded_msg = formatted_msg.encode("utf-8")
        print >> self.log_handle, encoded_msg
        print encoded_msg
        self.log_handle.flush()
        sys.stdout.flush()

    def shutdown(self):
        self.log("shutting down kgca")
        self.disconnect()
        self.close_log()

    def close_log(self):
        if self.log_handle is not None:
            self.log_handle.close()
            self.log_handle = None

    def wait_for(self, condition_func, check_interval=1.0):
        while not condition_func():
            self.poll_events()
            time.sleep(check_interval)

    def add_condition_callback(self, condition_func, callback_func, check_interval=1.0, dispose=True):
        event = Event(condition_func, callback_func, check_interval, self.get_time(), dispose)
        self.registered_events.append(event)

    def add_interval_callback(self, callback_func, interval=3):
        event = Event(None, callback_func, interval, self.get_time() + interval, dispose=False)
        self.registered_events.append(event)

    def remove_callback(self, callback_func):
        events_to_remove = []
        for event_index, event in enumerate(self.registered_events):
            if event.callback_func is callback_func:
                events_to_remove.append((event_index, event))

        if events_to_remove == []:
            self.log("Warning: no callback for %r to remove" % callback_func)

        for event_index, event in reversed(events_to_remove):
            actual_event = self.registered_events.pop(event_index)
            assert actual_event is event

    def wait(self, check_interval=1.0):
        self.poll_events()
        time.sleep(check_interval)

    def stage_resources(self):
        v = self.vessel()
        return v.resources_in_decouple_stage(v.control.current_stage, cumulative=False)

    def set_sas_mode(self, sas_mode):
        self.vessel().control.sas_mode = sas_mode
        self.wait()

    def v_at_radius(self, r, orbit):
        mu = orbit.body.gravitational_parameter
        a = orbit.semi_major_axis
        v = math.sqrt(mu * (2. / r - 1. / a))
        return v

    def rendezvous_transfer_time_semi_major(self, new_r):
        vessel = self.vessel()

        new_semi_major = (vessel.orbit.radius + new_r) / 2.

        mu = vessel.orbit.body.gravitational_parameter

        t = 2 * math.pi * math.sqrt(new_semi_major ** 3 / mu)
        return t

    def plan_lower_alt_rendezvous(self, alt, time_from_now=None):

        if time_from_now is None:
            time_from_now = 0

        vessel = self.vessel()
        surface_height = vessel.orbit.body.equatorial_radius

        # base_frame = vessel.orbit.body.non_rotating_reference_frame
        current_altitude = vessel.orbit.radius - surface_height

        self.log("- current alt %0.0f" % current_altitude)

        new_semi_major = (current_altitude + alt) / 2. + surface_height
        self.log("- new semi major %0.0f" % new_semi_major)

        # Plan lower circular burn (using vis-viva equation)
        mu = vessel.orbit.body.gravitational_parameter

        r = current_altitude + surface_height
        self.log("- apo %0.0f" % r)

        self.log("v2 = 2 / %0.0f - 1 / %0.0f" % (r, new_semi_major))
        a1 = vessel.orbit.semi_major_axis
        self.log("- cur semi major %0.0f" % a1)
        a2 = new_semi_major
        v1 = math.sqrt(mu * (2. / r - 1. / a1))
        v2 = math.sqrt(mu * (2. / r - 1. / a2))
        delta_v = v2 - v1

        self.log("v1 = %0.2f" % v1)
        self.log("v2 = %0.2f" % v2)
        self.log("delta_v = %0.2f" % delta_v)
        assert delta_v < 0
        node = vessel.control.add_node(self.get_simulation_time() + time_from_now, prograde=delta_v)

        burn_time = self.calculate_burn_time(delta_v, vessel)

        return node, burn_time

    def plan_increasing_circularization_burn(self):
        """
        Plans the circularization burn delta v, creates a maneuver node for it, and calculates the time to burn to
        do the maneuver
        :return: (node, burn time in s)
        """
        vessel = self.vessel()

        # Plan circularization burn (using vis-viva equation)
        self.log('Planning circularization burn')
        mu = vessel.orbit.body.gravitational_parameter
        r = vessel.orbit.apoapsis
        a1 = vessel.orbit.semi_major_axis
        a2 = r  # if the orbit is symmetric (what we want) the semi_major will be the apoapsis
        v1 = math.sqrt(mu * (2. / r - 1. / a1))
        v2 = math.sqrt(mu * (2. / r - 1. / a2))
        delta_v = v2 - v1
        node = vessel.control.add_node(self.get_simulation_time() + vessel.orbit.time_to_apoapsis, prograde=delta_v)

        burn_time = self.calculate_burn_time(delta_v, vessel)

        return node, burn_time

    def plan_decreasing_circularization_burn(self):
        """
        Plans the circularization burn delta v, creates a maneuver node for it, and calculates the time to burn to
        do the maneuver
        :return: (node, burn time in s)
        """
        vessel = self.vessel()

        # Plan circularization burn (using vis-viva equation)
        self.log('Planning decreased circularization burn')
        mu = vessel.orbit.body.gravitational_parameter
        r = vessel.orbit.periapsis
        a1 = vessel.orbit.semi_major_axis
        a2 = r  # if the orbit is symmetric (what we want) the semi_major will be the periapsis
        v1 = math.sqrt(mu * (2. / r - 1. / a1))
        v2 = math.sqrt(mu * (2. / r - 1. / a2))
        delta_v = v2 - v1
        node = vessel.control.add_node(self.get_simulation_time() + vessel.orbit.time_to_periapsis, prograde=delta_v)

        burn_time = self.calculate_burn_time(delta_v, vessel)

        return node, burn_time

    def add_burn_node(self, seconds_from_now, normal=0, prograde=0, retrograde=0):
        """
        args are e.g.:
        prograde = delta_v
        """
        vessel = self.vessel()
        node = vessel.control.add_node(self.get_simulation_time() + seconds_from_now,
                                       normal=normal,
                                       prograde=prograde)
        delta_v = abs(normal) + abs(prograde)
        burn_time = self.calculate_burn_time(delta_v, vessel)

        return node, burn_time

    def calculate_burn_time(self, delta_v, vessel=None):
        delta_v = abs(delta_v)
        if vessel is None:
            vessel = self.vessel()
        # Calculate burn time (using rocket equation)
        f = vessel.available_thrust
        isp = vessel.specific_impulse * 9.82  # isn't that constant actually g, a body value?
        m0 = vessel.mass
        m1 = m0 / math.exp(delta_v / isp)
        flow_rate = f / isp
        burn_time = (m0 - m1) / flow_rate
        return burn_time

    def true_anomaly(self, orbit):
        e = orbit.eccentricity
        ea = orbit.eccentric_anomaly
        return true_anomaly_calc(e, ea)

    def orbital_plane_change_delta_v(self, ascending_angle_radians):
        orbit = self.vessel().orbit
        e = orbit.eccentricity
        w = orbit.argument_of_periapsis
        f = self.true_anomaly(orbit)
        n = 1. / orbit.period
        a = orbit.semi_major_axis

        numerator = 2 * math.sin(ascending_angle_radians/2.) * math.sqrt(1. - e*e) * math.cos(w + f) * n * a
        denominator = 1 + e * math.cos(f)
        v = numerator/denominator
        return v


def launch(a):
    assert isinstance(a, Automator)

    have_gantry = HAVE_GANTRY

    # noinspection PyPep8Naming
    SpaceCenter = a.conn.space_center.__class__
    # noinspection PyPep8Naming
    SAS_MODE_STABILITY_ASSIST = SpaceCenter.SASMode.stability_assist
    # noinspection PyPep8Naming
    SAS_MODE_PROGRADE = SpaceCenter.SASMode.prograde
    # noinspection PyPep8Naming
    SAS_MODE_RETROGRADE = SpaceCenter.SASMode.retrograde
    # noinspection PyPep8Naming
    SAS_MODE_TARGET = SpaceCenter.SASMode.target
    # noinspection PyPep8Naming
    SAS_MODE_MANEUVER = SpaceCenter.SASMode.maneuver

    vessel = a.vessel()

    print vessel

    ap = vessel.auto_pilot
    # assert isinstance(ap, krpc.types.AutoPilot)

    a.log("## initial setup")

    a.log("== enable sas")
    vessel.control.sas = True
    a.wait()
    a.set_sas_mode(SAS_MODE_STABILITY_ASSIST)

    a.log("== setup ap")

    ap.target_pitch_and_heading(90, 90)
    ap.engage()
    vessel.control.throttle = 1

    a.log("-- pause before launch")

    a.wait()

    class RelayFlags(object):
        def __init__(self):
            self.boosters_done = False
            self.first_stage_done = False
            self.burn_throttle = None

    flags = RelayFlags()

    a.log("## launching")
    vessel.control.activate_next_stage()

    a.log("== engine start")

    if have_gantry:
        a.wait(0.3)
        a.log("== gantry disconnect")
        vessel.control.activate_next_stage()

    def on_overspeed():
        a.log("** going too fast at low alt")
        prev_throttle = vessel.control.throttle
        if prev_throttle != 0:
            a.log("-- lower throttle to 50%")
            vessel.control.throttle = 0.5

    a.log("-- set up overspeed event")

    alt = lambda: vessel.flight().mean_altitude
    speed = lambda: norm(vessel.velocity(vessel.orbit.body.reference_frame))

    a.add_condition_callback(lambda: alt() < 60000 and speed() > 310,
                             on_overspeed)

    def on_solid_booster_complete():
        a.log("** solid fuel exhausted")
        prev_throttle = vessel.control.throttle
        if prev_throttle != 0:
            a.log("-- pause throttle")
            vessel.control.throttle = 0

        a.wait(1)

        a.log("** separating boosters")
        vessel.control.activate_next_stage()

        if prev_throttle != 0:
            a.wait(2)
            a.log("-- resuming throttle")
            # vessel.control.throttle = prev_throttle
            vessel.control.throttle = 1

        flags.boosters_done = True

    a.log("-- set up solid booster completion event")
    a.add_condition_callback(lambda: vessel.resources.amount('SolidFuel') <= 0.1, on_solid_booster_complete)

    def on_first_stage_complete():
        a.log("** liquid fuel exhausted")
        a.wait(1)

        a.log("** separating from first stage")
        vessel.control.activate_next_stage()

        a.wait(2)
        a.log("** starting second stage")
        vessel.control.activate_next_stage()

        flags.first_stage_done = True

    a.log("-- set up second stage liquid completion event")
    a.add_condition_callback(lambda: a.stage_resources().amount("LiquidFuel") < 0.1 and flags.boosters_done,
                             on_first_stage_complete)

    a.log("-- set up stats display")

    def display_stats():
        if not flags.boosters_done:
            solid_fuel_amount = vessel.resources.amount('SolidFuel')
            a.log("Solid fuel: %r" % solid_fuel_amount)
        if flags.boosters_done and not flags.first_stage_done:
            sr = a.stage_resources()
            liquid_fuel_amount = sr.amount('LiquidFuel')
            a.log("Liquid fuel: %r" % liquid_fuel_amount)

        a.log("alt %0.0f speed %0.2f" % (alt(), speed()))

        raw = {"vessel": vessel}

        stats_wanted = ["vessel.orbit.time_to_apoapsis",
                        "vessel.orbit.apoapsis_altitude",
                        "vessel.orbit.periapsis_altitude",
                        "vessel.orbit.time_to_periapsis",
                        ]

        parts = []
        for fully_qualified_name in stats_wanted:
            entry = fully_qualified_name.split(".")
            cur = raw[entry[0]]
            for name in entry[1:]:
                cur = getattr(cur, name)

            if type(cur) == float:
                parts.append("%s: %0.2f" % (entry[-1], cur))
            else:
                parts.append("%s: %r" % (entry[-1], cur))

        a.log(", ".join(parts))

    a.add_interval_callback(display_stats, 5)

    a.log("-- waiting for turn altitude %d" % FIRST_MANEUVER_ALTITUDE)
    a.wait_for(lambda: vessel.flight().mean_altitude >= FIRST_MANEUVER_ALTITUDE)

    a.log("## gravity turns")
    a.log("-- pitch 10 deg")
    vessel.auto_pilot.target_pitch_and_heading(80, 90)
    a.wait_for(lambda: vessel.flight().mean_altitude >= FIRST_MANEUVER_ALTITUDE + 5000)
    a.log("-- pitch 20 deg")
    vessel.auto_pilot.target_pitch_and_heading(70, 90)
    a.wait_for(lambda: vessel.flight().mean_altitude >= FIRST_MANEUVER_ALTITUDE + 10000)
    a.log("-- pitch 30 deg")
    vessel.auto_pilot.target_pitch_and_heading(60, 90)
    a.wait_for(lambda: vessel.flight().mean_altitude >= FIRST_MANEUVER_ALTITUDE + 15000)

    a.log("-- waiting for target apo alt %d" % TARGET_ORBIT_ALTITUDE)
    a.wait_for(lambda: vessel.orbit.apoapsis_altitude >= TARGET_ORBIT_ALTITUDE - SLOW_ALTITUDE_MARGIN,
               check_interval=0.125)
    a.log("-- getting close to target altitude; slowing burn")
    vessel.control.throttle = 0.5
    a.wait_for(lambda: vessel.orbit.apoapsis_altitude >= TARGET_ORBIT_ALTITUDE,
               check_interval=0.125)

    a.log("== main engines stop")
    vessel.control.throttle = 0
    ap.disengage()

    a.log("== preparing for orbital burn")

    circularize(a)

    a.remove_callback(display_stats)
    a.log("## orbit established")


def circularize(a, increase=True):
    vessel = a.vessel()

    a.log("-- enabling SAS")
    vessel.control.sas = True
    a.wait()
    a.log("-- calculating burn")
    if increase:
        node, burn_time = a.plan_increasing_circularization_burn()
    else:
        node, burn_time = a.plan_decreasing_circularization_burn()
    try:
        a.wait()

        SpaceCenter = a.conn.space_center.__class__
        SAS_MODE_MANEUVER = SpaceCenter.SASMode.maneuver

        a.set_sas_mode(SAS_MODE_MANEUVER)

        burn_start_ahead = max(0.5, burn_time / 2.0) + 5  # fudge factor

        if increase:
            a.log("== waiting to reach %0.2f s before apoapsis" % burn_start_ahead)
            a.wait_for(lambda: vessel.orbit.time_to_apoapsis <= burn_start_ahead)
        else:
            a.log("== waiting to reach %0.2f s before periapsis" % burn_start_ahead)
            a.wait_for(lambda: vessel.orbit.time_to_periapsis <= burn_start_ahead)

        a.log("## orbit burn")
        burn_maneuver_now(a, node)
    finally:
        node.remove()


def burn_maneuver_now(a, node, ignore_maneuver_node_check=False):

    vessel = a.vessel()

    remaining_burn_v_vector = lambda: node.remaining_burn_vector(node.reference_frame)
    remaining_burn_v = lambda: norm(node.remaining_burn_vector(node.reference_frame))

    base_frame = vessel.orbit.body.non_rotating_reference_frame
    direction_maneuver_angle = angle_between(vessel.direction(node.reference_frame), remaining_burn_v_vector())
    a.log("-- angle to maneuver direction: %0.2f (%0.2f deg)" % (direction_maneuver_angle,
                                                                 math.degrees(direction_maneuver_angle)))
    if not ignore_maneuver_node_check:
        if not direction_maneuver_angle < 0.03:
            a.log("wanted angle < 0.03 but angle was higher (see above)")

    # originally we just burned using time:

    # time.sleep(burn_time - 0.2)
    # end_time = a.get_time()
    # a.wait()

    # now we actually use the node's remaining delta v indication

    pointing_away_from_burn = lambda: pointing_apart(vessel.direction(node.reference_frame), remaining_burn_v_vector())

    if remaining_burn_v() > 100.0:
        a.log("-- Full throttle burn")

        vessel.control.throttle = 1

        a.wait_for(lambda: remaining_burn_v() < 100.0 or pointing_away_from_burn(), check_interval=0.125)

    if remaining_burn_v() > 10.0:
        a.log('-- Slow burn')
        vessel.control.throttle = 0.25

        a.wait_for(lambda: remaining_burn_v() < 10.0 or pointing_away_from_burn(), check_interval=0.125)

    a.log('-- Fine tuning burn')
    vessel.control.throttle = 0.05

    a.wait_for(lambda: remaining_burn_v() <= 0.0 or pointing_away_from_burn(), check_interval=0.125)

    vessel.control.throttle = 0

    a.wait()

    # if False:  # old ad-hoc orbital burn stuff
    #
    #     a.log("== waiting to reach %r seconds before apoapsis" % TARGET_APOAPSIS_LEADING_TIME)
    #     a.wait_for(lambda: vessel.orbit.time_to_apoapsis <= TARGET_APOAPSIS_LEADING_TIME)
    #
    #     def on_time_low():
    #         if vessel.control.throttle != flags.burn_throttle:
    #             a.log("-- cycling on")
    #             vessel.control.throttle = flags.burn_throttle
    #
    #     def on_time_high():
    #         if vessel.control.throttle != 0:
    #             a.log("-- cycling off")
    #             vessel.control.throttle = 0
    #
    #     a.add_condition_callback(lambda: vessel.orbit.time_to_apoapsis <
    #                              TARGET_APOAPSIS_LEADING_TIME - TARGET_APOAPSIS_LEADING_TIME_MARGIN,
    #                              on_time_low, dispose=False, check_interval=0.125)
    #     a.add_condition_callback(lambda: TARGET_APOAPSIS_LEADING_TIME + TARGET_APOAPSIS_LEADING_TIME_MARGIN <
    #                              vessel.orbit.time_to_apoapsis < 500,  # max also in case we get past it and it wraps
    #                              on_time_high, dispose=False, check_interval=0.125)
    #
    #     def slow_zone():
    #         a.log("-- close to target altitude; slowing throttle")
    #         lower_throttle = 0.1
    #         was_on = vessel.control.throttle > lower_throttle
    #         flags.burn_throttle = lower_throttle
    #         if was_on:
    #             vessel.control.throttle = flags.burn_throttle
    #
    #     a.add_condition_callback(lambda: vessel.orbit.periapsis_altitude >= TARGET_ORBIT_ALTITUDE - SLOW_ALTITUDE_MARGIN,
    #                              slow_zone, check_interval=0.125)
    #
    #     a.wait_for(lambda: vessel.orbit.periapsis_altitude >= TARGET_ORBIT_ALTITUDE, check_interval=0.125)
    #
    #     a.remove_callback(slow_zone)
    #     a.remove_callback(on_time_low)
    #     a.remove_callback(on_time_high)
    #
    #     vessel.control.throttle = 0
    #
    #     a.log("## orbit burn complete")
    #
    #     a.log("-- extra stop")
    #     a.wait()
    #
    #     vessel.control.throttle = 0
    #
    #     a.log("## circularization burn segment")
    #     altitude_margin = 2000
    #     if vessel.orbit.apoapsis_altitude > TARGET_ORBIT_ALTITUDE + altitude_margin:
    #         a.log("-- apoapsis above target; we need to slow at periapsis")
    #         slow_at_periapsis = True
    #     else:
    #         slow_at_periapsis = False
    #
    #     if slow_at_periapsis:
    #         a.log("-- switching to retrograde")
    #         a.set_sas_mode(SAS_MODE_RETROGRADE)
    #
    #         prev_periapsis_altitude = vessel.orbit.periapsis_altitude
    #
    #         a.log("-- waiting for periapsis")
    #         a.wait_for(lambda: vessel.orbit.time_to_periapsis < 3)
    #
    #         a.log("## circularization burn")
    #         vessel.control.throttle = 0.05
    #
    #         if slow_at_periapsis:
    #             if prev_periapsis_altitude < TARGET_ORBIT_ALTITUDE:
    #                 a.log("-- the old periapsis is low, so just stop the apo when it reaches the bound")
    #                 a.wait_for(lambda: vessel.orbit.apoapsis_altitude < TARGET_ORBIT_ALTITUDE + altitude_margin)
    #             else:
    #                 a.log("-- the periapsis is high, so the apo will become the peri; stop when it gets low")
    #                 a.wait_for(lambda: vessel.orbit.periapsis_altitude < TARGET_ORBIT_ALTITUDE - altitude_margin)
    #
    #         vessel.control.throttle = 0


def cross_product(x, y):
    """
    >>> a = (3, -3, 1)
    >>> b = (4, 9, 2)
    >>> cross_product(a, b)
    (-15, -2, 39)
    """
    x1, x2, x3 = x
    y1, y2, y3 = y
    z1 = x2 * y3 - x3 * y2
    z2 = x3 * y1 - x1 * y3
    z3 = x1 * y2 - x2 * y1
    return z1, z2, z3


def dot_product(u, v):
    """
    >>> a = (6, -1, 3)
    >>> b = (4, 18, -2)
    >>> dot_product(a, b)
    0
    """
    return u[0] * v[0] + u[1] * v[1] + u[2] * v[2]


def norm(u):
    u1, u2, u3 = u
    return math.sqrt(u1*u1 + u2*u2 + u3*u3)


def normalize(u):
    return vec_mult(u, 1./norm(u))


def vec_sum(u, v):
    return tuple(ux + vx for (ux, vx) in zip(u, v))


def vec_mult(u, m):
    return tuple(m * ux for ux in u)


def vec_round(u, ndigits):
    return tuple(round(ux, ndigits) for ux in u)


AROUND_X_AXIS = 0
AROUND_Y_AXIS = 1
AROUND_Z_AXIS = 2


def rotate(u, theta, around_axis):
    """
    >>> a = (1, 0, 0)
    >>> vec_round(rotate(a, math.pi / 2.0, 2), 1)
    (0.0, -1.0, 0.0)
    >>> vec_round(rotate(a, math.pi / 4.0, 2), 1)
    (0.7, -0.7, 0.0)
    >>> vec_round(rotate(a, math.pi, 2), 1)
    (-1.0, -0.0, 0.0)
    >>> a = (1, 0, 0)
    >>> vec_round(rotate(a, math.pi / 2.0, 1), 1)
    (0.0, 0.0, -1.0)
    >>> a = (1, -1, 0)
    >>> vec_round(rotate(a, math.pi / 2.0, 2), 1)
    (-1.0, -1.0, 0.0)
    """
    rotated_param_indexes = range(3)
    rotated_param_indexes.pop(around_axis)

    rotated_params = [u[i] for i in rotated_param_indexes]

    output = list(u)

    output[rotated_param_indexes[0]] = rotated_params[0] * math.cos(theta) + rotated_params[1] * math.sin(theta)
    output[rotated_param_indexes[1]] = - rotated_params[0] * math.sin(theta) + rotated_params[1] * math.cos(theta)

    return tuple(output)


def orbital_plane_normal_vector(orbit):
    u = (0, 1, 0)
    inclination = orbit.inclination
    if inclination != 0:
        u = rotate(u, inclination, AROUND_X_AXIS)
        ascending = orbit.longitude_of_ascending_node
        u = rotate(u, ascending, AROUND_Y_AXIS)
    return u


def swap_xz(u):
    u = list(u)
    # temp = u[0]
    # u[0] = -u[2]
    # u[2] = temp
    u[2] = -u[2]
    return tuple(u)


def angle_between(u, v):
    return math.acos(dot_product(u, v) / (norm(u) * norm(v)))


def pointing_apart(u, v):
    return angle_between(u, v) > math.pi / 2.


def fix_inclination(a):
    assert isinstance(a, Automator)

    SpaceCenter = a.conn.space_center.__class__
    SAS_MODE_NORMAL = SpaceCenter.SASMode.normal
    SAS_MODE_ANTINORMAL = SpaceCenter.SASMode.anti_normal
    SAS_MODE_MANEUVER = SpaceCenter.SASMode.maneuver

    # at the descending / ascending node, redirect the magnitude of the current velocity to the target direction.

    cur_vessel = a.vessel()
    target_vessel = a.target_vessel()
    if target_vessel is None:
        a.log("No target vessel is selected; please select a target to match orbits with")
        raise Exception("no target selected")

    assert cur_vessel.orbit.body.name == target_vessel.orbit.body.name

    base_frame = cur_vessel.orbit.body.non_rotating_reference_frame

    target_plane_normal = orbital_plane_normal_vector(target_vessel.orbit)
    cur_plane_normal = orbital_plane_normal_vector(cur_vessel.orbit)

    # the cross product of these two vectors is along the ascending node - descending node line, and then angle between
    # them is the ascending/descending node angle

    descending_node_axis = swap_xz(normalize(cross_product(target_plane_normal, cur_plane_normal)))

    ascending_angle_radians = angle_between(target_plane_normal, cur_plane_normal)

    def pretty(v):
        return "(%0.2f, %0.2f, %0.2f)" % v

    a.log("target plane normal %s (%r)" % (pretty(target_plane_normal), norm(target_plane_normal)))
    a.log("cur plane normal %s (%r)" % (pretty(cur_plane_normal), norm(cur_plane_normal)))
    a.log("descending node axis: %s" % pretty(descending_node_axis))

    a.log("ascending angle %r (%r degrees)" % (ascending_angle_radians, math.degrees(ascending_angle_radians)))

    # let's figure out radius at that angle, and then we can figure out the velocity vector

    # v = a.orbital_plane_change_delta_v(ascending_angle_radians)
    r = norm(cur_vessel.position(base_frame))
    mu = cur_vessel.orbit.body.gravitational_parameter
    v = simple_inclination_change_calc(r, ascending_angle_radians, mu)

    a.log("required delta v: %r" % v)

    a.log("-- What inclination node is up next?")

    # let's keep some time to maneuver
    maneuver_time = 90.
    maneuver_orbit_angle = maneuver_time / cur_vessel.orbit.period * 2. * math.pi

    a.log("maneuver time reserved %0.2f s (%0.2f - %0.2f deg)" % (maneuver_time, maneuver_orbit_angle,
                                                                  math.degrees(maneuver_orbit_angle)))

    orbital_frame = cur_vessel.orbital_reference_frame

    orbital_frame_descending_node_axis = a.conn.space_center.transform_direction(descending_node_axis,
                                                                                 base_frame, orbital_frame)
    print pretty(orbital_frame_descending_node_axis)
    orbital_frame_backed_up_descending_node_axis = rotate(orbital_frame_descending_node_axis, -maneuver_orbit_angle,
                                                          AROUND_Z_AXIS)
    print pretty(orbital_frame_backed_up_descending_node_axis)
    print angle_between(orbital_frame_descending_node_axis, orbital_frame_backed_up_descending_node_axis)
    backed_up_descending_node_axis = a.conn.space_center.transform_direction(orbital_frame_backed_up_descending_node_axis,
                                                                             orbital_frame, base_frame)

    a.log("descending node axis: %s" % pretty(descending_node_axis))
    a.log("backed up descending node axis: %s" % pretty(backed_up_descending_node_axis))

    # are we going toward the descending node's axis?
    cur_velocity = cur_vessel.velocity(base_frame)

    a.log("cur velocity (base frame): %s" % pretty(cur_velocity))

    v_angle_with_descending_node = angle_between(descending_node_axis, cur_velocity)
    a.log("-- velocity angle to descending node axis: %0.2f (%0.2f deg)" % (v_angle_with_descending_node,
                                                                            math.degrees(v_angle_with_descending_node)))
    v_angle_with_descending_node = angle_between(backed_up_descending_node_axis, cur_velocity)
    a.log("-- backed up velocity angle to descending node axis: %0.2f (%0.2f deg)" % (v_angle_with_descending_node,
                                                                            math.degrees(v_angle_with_descending_node)))

    ascending_node_up_next = v_angle_with_descending_node > math.pi / 2.

    if ascending_node_up_next:
        a.log("ascending node - antinormal burn")
        target_node_axis = vec_mult(descending_node_axis, -1)
        a.set_sas_mode(SAS_MODE_ANTINORMAL)
        point_to = (0, 0, -1)
    else:
        a.log("descending node - normal burn")
        target_node_axis = descending_node_axis
        a.set_sas_mode(SAS_MODE_NORMAL)
        point_to = (0, 0, 1)

    a.log("-- waiting for turn to complete")

    a.wait_for(lambda: angle_between(cur_vessel.direction(cur_vessel.orbital_reference_frame), point_to) < 0.03 and
               norm(cur_vessel.angular_velocity(cur_vessel.orbital_reference_frame)) < 0.01)

    a.log("-- turn complete")

    # maneuver_end = time.time() + maneuver_time
    # a.wait_for(lambda: time.time() >= maneuver_end)

    a.log("-- waiting for the ship to get to the maneuver location")

    def show_position():
        pos = cur_vessel.position(base_frame)
        d = angle_between(pos, descending_node_axis)
        print "pos %s target %s angle %0.2f (%0.2f degrees) ecc %0.4f ta %0.4f" % (pretty(normalize(pos)),
                                                                                   pretty(descending_node_axis),
                                                                                   d, math.degrees(d),
                                                                                   cur_vessel.orbit.eccentricity,
                                                                                   a.true_anomaly(cur_vessel.orbit),
                                                                                   )

    a.add_interval_callback(show_position, interval=5)

    # wait for us to get to around 0 deg (descending node) or 180 deg (ascending node)
    a.wait_for(lambda: angle_between(cur_vessel.position(base_frame), target_node_axis) < 0.03)

    # v = a.orbital_plane_change_delta_v(ascending_angle_radians)
    r = norm(cur_vessel.position(base_frame))
    mu = cur_vessel.orbit.body.gravitational_parameter
    v = simple_inclination_change_calc(r, ascending_angle_radians, mu)

    assert v >= 0

    if angle_between(cur_vessel.position(base_frame), descending_node_axis) > math.pi / 2.:
        # angle > 90 deg
        a.log("reached ascending node - antinormal burn")
        node, burn_time = a.add_burn_node(0, normal=-v)
    else:
        a.log("reached descending node - normal burn")
        node, burn_time = a.add_burn_node(0, normal=v)
    try:
        a.log("required delta v in the direction: %r" % v)

        a.set_sas_mode(SAS_MODE_MANEUVER)

        burn_maneuver_now(a, node, ignore_maneuver_node_check=True)
    finally:
        node.remove()


def run_doctests():
    doctest.testmod()


def pause(msg):
    print msg
    sys.stdin.readline()


def orb_direction_to_angle(p, y_param=2, x_param=0):
    # equator_meridian = (1, 0, 0)
    # equator_positive_turn = (0, 0, 1)
    """
    >>> '%0.2f' % math.degrees(orb_direction_to_angle((1,0,0)))
    '0.00'
    >>> '%0.2f' % math.degrees(orb_direction_to_angle((0,0,1)))
    '90.00'
    >>> '%0.2f' % math.degrees(orb_direction_to_angle((-1,0,0)))
    '180.00'
    >>> '%0.2f' % math.degrees(orb_direction_to_angle((0,0,-1)))
    '270.00'
    >>> '%0.2f' % math.degrees(orb_direction_to_angle((0,1,0), y_param=0, x_param=1))
    '0.00'
    >>> '%0.2f' % math.degrees(orb_direction_to_angle((1,0,0), y_param=0, x_param=1))
    '90.00'
    >>> '%0.2f' % math.degrees(orb_direction_to_angle((0,-1,0), y_param=0, x_param=1))
    '180.00'
    >>> '%0.2f' % math.degrees(orb_direction_to_angle((-1,0,0), y_param=0, x_param=1))
    '270.00'
    """
    return math.atan2(p[y_param], p[x_param]) % (2. * math.pi)


def rendezvous(a):
    assert isinstance(a, Automator)

    a.log("# rendezvous burn")

    cur_vessel = a.vessel()
    target_vessel = a.target_vessel()

    assert cur_vessel.orbit.body.name == target_vessel.orbit.body.name

    # TODO check orbits are close to circular and in plane and in the same direction

    a.log("- target info")
    a.log("target apo alt %0.0f" % target_vessel.orbit.apoapsis_altitude)
    a.log("target peri alt %0.0f" % target_vessel.orbit.periapsis_altitude)
    new_r = (target_vessel.orbit.apoapsis + target_vessel.orbit.periapsis) / 2.
    alt = (target_vessel.orbit.apoapsis_altitude + target_vessel.orbit.periapsis_altitude) / 2.

    a.log("- planning rendezvous burn for alt %0.0f" % alt)

    transfer_orbit_period = a.rendezvous_transfer_time_semi_major(new_r)

    a.log("- the period of the transfer orbit is %0.2f seconds" % transfer_orbit_period)

    transfer_time = transfer_orbit_period / 2.  # transfer takes half an orbit

    a.log("- the transfer will take %0.2f seconds (%0.2f hours)" % (transfer_time,
                                                                    transfer_time / 3600.))

    # Calculate the amount of time we need to wait for the orbits to be in sync

    # For a first approximation, just use the angle and orbital periods to figure it out
    # TODO a real implementation for this part

    vessels_by_speed = [cur_vessel, target_vessel]
    vessels_by_speed.sort(key=lambda vessel: vessel.orbit.period)

    fast_vessel, slow_vessel = vessels_by_speed

    base_frame = cur_vessel.orbit.body.non_rotating_reference_frame

    if fast_vessel is cur_vessel:
        a.log("cur vessel is catching up")
    else:
        a.log("target vessel is catching up")

    fast_vessel_angle = orb_direction_to_angle(a.conn.space_center.transform_direction(fast_vessel.position(base_frame),
                                                                                       base_frame,
                                                                                       cur_vessel.orbital_reference_frame),
                                               y_param=0, x_param=1)
    slow_vessel_angle = orb_direction_to_angle(a.conn.space_center.transform_direction(slow_vessel.position(base_frame),
                                                                                       base_frame,
                                                                                       cur_vessel.orbital_reference_frame),
                                               y_param=0, x_param=1)

    a.log("fast vessel angle %0.2f (%0.2f deg)" % (fast_vessel_angle, math.degrees(fast_vessel_angle)))
    a.log("slow vessel angle %0.2f (%0.2f deg)" % (slow_vessel_angle, math.degrees(slow_vessel_angle)))

    # how much angle the fast vessel must go through to 'catch up' to the slow vessel
    angle_change_required = (slow_vessel_angle - fast_vessel_angle) % (2. * math.pi)

    a.log("angle change to catch up %0.2f (%0.2f deg)" % (angle_change_required, math.degrees(angle_change_required)))

    fast_vessel_mean_motion = 2. * math.pi / fast_vessel.orbit.period  # radians per second
    slow_vessel_mean_motion = 2. * math.pi / slow_vessel.orbit.period
    delta_mean_motion = fast_vessel_mean_motion - slow_vessel_mean_motion

    a.log("catching up at %0.5f (%0.7f deg) per second before transfer" % (delta_mean_motion,
                                                                           math.degrees(delta_mean_motion)))

    cur_vessel_transfer_mean_motion = 2. * math.pi / transfer_orbit_period
    if fast_vessel is cur_vessel:
        transfer_delta_mean_motion = cur_vessel_transfer_mean_motion - slow_vessel_mean_motion
    else:
        transfer_delta_mean_motion = fast_vessel_mean_motion - cur_vessel_transfer_mean_motion

    a.log("catching up at %0.5f (%0.7f deg) per second during transfer" % (transfer_delta_mean_motion,
                                                                           math.degrees(transfer_delta_mean_motion)))

    angle_change_during_transfer = transfer_delta_mean_motion * transfer_time

    a.log("angle catch up during transfer: %0.2f (%0.2f deg)" % (angle_change_during_transfer,
                                                                 math.degrees(angle_change_during_transfer)))

    angle_change_required_before_transfer = angle_change_required - angle_change_during_transfer
    a.log("angle catch up before transfer: %0.2f (%0.2f deg)" % (angle_change_required_before_transfer,
                                                                 math.degrees(angle_change_required_before_transfer)))

    catch_up_time_before_transfer = angle_change_required_before_transfer / delta_mean_motion
    a.log("rotation catch up time before transfer %0.2f seconds (%0.2f hours)" % (catch_up_time_before_transfer,
                                                                                  catch_up_time_before_transfer / 3600.))
    rotation_catch_up_time = catch_up_time_before_transfer + transfer_time

    a.log("total rotation catch up time %0.2f seconds (%0.2f hours)" % (rotation_catch_up_time,
                                                                  rotation_catch_up_time / 3600.))

    # working backwards
    # - the vessels meet
    # - the transfer
    #     the target is going at its mean motion and the current is going at orbital transfer mean motion
    #     we do the transfer time worth of the mean motions
    # - catch up time
    #     the target and current are going at their mean motions

    node, burn_time = a.plan_lower_alt_rendezvous(alt, time_from_now=catch_up_time_before_transfer)

    a.wait()

    SpaceCenter = a.conn.space_center.__class__
    # noinspection PyPep8Naming
    SAS_MODE_RETROGRADE = SpaceCenter.SASMode.retrograde

    a.set_sas_mode(SAS_MODE_RETROGRADE)

    point_to = (0, -1, 0)

    burn_start_ahead = max(0.5, burn_time / 2.0) + 0  # fudge factor

    cur_time = a.get_simulation_time()
    sim_time_to_start_burn = cur_time + catch_up_time_before_transfer - burn_start_ahead
    sim_time_of_rendezvous = cur_time + rotation_catch_up_time

    a.log("-- waiting for turn to complete")

    a.wait_for(lambda: angle_between(cur_vessel.direction(cur_vessel.orbital_reference_frame), point_to) < 0.03 and
                       norm(cur_vessel.angular_velocity(cur_vessel.orbital_reference_frame)) < 0.01)

    a.log("== waiting to burn: %0.2f s" % catch_up_time_before_transfer)

    a.wait_for(lambda: a.get_simulation_time() >= sim_time_to_start_burn)

    a.log("## rendezvous burn")
    burn_maneuver_now(a, node)

    a.log("- burn complete")
    a.log("- waiting for rendezvous time")

    a.wait_for(lambda: a.get_simulation_time() >= sim_time_of_rendezvous)

    a.log("# rendezvous complete")


def main():

    run_doctests()

    a = Automator()
    try:
        target_vessel = a.target_vessel()
        if target_vessel is None:
            a.log("No target vessel is selected; please select a target to match orbits with")
            raise Exception("no target selected")

        # launch(a)
        # fix_inclination(a)

        rendezvous(a)

        circularize(a, increase=False)

    finally:
        a.shutdown()


if __name__ == "__main__":
    main()
